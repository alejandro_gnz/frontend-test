# Frubana Frontend - test

prueba para desarrollador frontend de frubana.

### Requisitos
* nodejs
* yarn o npm

### iniciar el proyecto
* clonar el repositoio

  ``` bash
  $ git clone https://gitlab.com/alejandro_gnz/frontend-test.git
  ```
* instalar dependencias
  ``` bash
  $ cd carpeta-del-proyecto
  $ yarn install
  ```
* iniciar el servidor
  ``` bash
  $ yarn start
  ```